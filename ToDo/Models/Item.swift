//
//  Item.swift
//  ToDo
//
//  Created by Asad Jamil on 8/17/18.
//  Copyright © 2018 Asad Jamil. All rights reserved.
//

import Foundation
import RealmSwift
class Item: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var done: Bool = false
    @objc dynamic var dateCreated: Date? 
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
