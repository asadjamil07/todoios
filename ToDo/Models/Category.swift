//
//  Category.swift
//  ToDo
//
//  Created by Asad Jamil on 8/17/18.
//  Copyright © 2018 Asad Jamil. All rights reserved.
//

import Foundation
import RealmSwift
class Category: Object {
    @objc dynamic var name: String = ""
    let items = List<Item>()
}
